module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'dist/css/style.css': 'src/css/index.scss'
        }
      }
    },
    autoprefixer: {
      dist: {
        files: {
          'dist/css/style.css': 'dist/css/style.css'
        }
      }
    },
    jshint: {
      files: ['src/**/*.js'],
      options: {
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    concat: {
      options: {
        separator: ';\n'
      },
      dist: {
        src: [
          'src/**/*.js',
          '!src/js/vendor/*.js',
          'src/main.js',
        ],
        dest: 'dist/js/main.min.js'
      }
    },
    copy: {
      main: {
        expand: true,
        cwd: 'src/js/vendor/',
        src: '**',
        dest: 'dist/js/vendor/',
        flatten: true,
        filter: 'isFile',
      },
    },
    uglify: {
      dist: {
        files: {
          'dist/js/main.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },
    watch: {
      files: [
        'Gruntfile.js',
        'src/css/**/*.scss',
        'src/**/*.js',
        'src/**/*.html'
      ],
      options: {
        atBegin: true,
      },
      tasks: [
        'sass',
        'autoprefixer',
        'concat',
        'copy',
        'uglify'
      ]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', ['watch']);
  grunt.registerTask('w', ['watch']);
  grunt.registerTask('hint', ['jshint']);

};
