# Pigsty

A watcher with:
  - SASS
  - Autoprefixer

### Installation
- Clone repo or download zip and place contents where you need
- `cd` to that directory
- `npm install`

### Usage
- `grunt`
  - `w` [watch] - compile SASS, concat all JS, minify all JS
  - `hint` [jshint]
